const SERVER_URL = `ws://${window.location.hostname}:9600`;
const MESSAGE_HISTORY_SIZE = 200;

let username = "";
while (username.toString().trim().length < 2) {
  username = window.prompt("Enter your username:", "") ?? "";
}

// Get useful elements
const chatForm = getById("chat-form");
const messageInput = getById("prompt");
const sendButton = getById("send-button");
const messageContainer = getById("chat-messages");

const websocket = new WebSocket(SERVER_URL);

websocket.onopen = (event) => {
  console.log("Websocket opened.", event);
  addSystemMessage("Connected!", "green");

  const data = {
    type: "join",
    username: username,
  };

  websocket.send(JSON.stringify(data));
};

websocket.onclose = (event) => {
  console.log("Websocket closed.", event);
  addSystemMessage("Lost connection.", "red");
  addReloadButton();
  disableChat();
};

websocket.onerror = (event) => {
  console.error("Websocket error.", event);
  addSystemMessage("Connection failed.", "red");
  addReloadButton();
  disableChat();
};

websocket.onmessage = (event) => {
  try {
    const data = JSON.parse(event.data);

    if (data.type == "message") {
      addUserMessage(data.username, data.content);
    } else if (data.type == "system") {
      addSystemMessage(data.content, "blue");
    }
  } catch (e) {
    console.error(e, event);
  }
};

chatForm.addEventListener("submit", (event) => {
  event.preventDefault();

  const content = messageInput.value;
  const trimmedContent = content.toString().trim();

  if (trimmedContent.length > 0) {
    processMessage(trimmedContent);
  }

  messageInput.value = "";
});

// Keep chat input focused
messageInput.focus();
addEventListener("keydown", () => {
  messageInput.focus();
});

/**
 * Process an input message
 * @param {string} raw Raw message content
 */
function processMessage(raw) {
  let data;

  // client-side clear command
  if (raw === "/clear") {
    clearMessages();
    return;
  }

  if (raw.startsWith("/")) {
    data = {
      type: "command",
      content: raw.substring(1).trimStart(),
    };
  } else {
    data = {
      type: "message",
      content: raw,
    };
  }

  websocket.send(JSON.stringify(data));
}

/**
 * Add a system message to the page
 * @param {string} messageContent
 * @param {string} color
 */
function addSystemMessage(messageContent, color = "") {
  const div = document.createElement("div");
  div.className = `message ${color}`;
  div.textContent = messageContent;

  appendMessageElement(div);
}

/**
 * Add a user message to the page
 * @param {string} username
 * @param {string} messageContent
 */
function addUserMessage(username, messageContent) {
  const div = document.createElement("div");
  div.className = "message";

  const span = document.createElement("span");
  span.className = "username";
  span.textContent = `${username}: `;

  div.appendChild(span);
  div.insertAdjacentText("beforeend", messageContent);

  appendMessageElement(div);
}

/**
 * Add page reload button
 */
function addReloadButton() {
  const button = document.createElement("button");
  button.type = "button";
  button.innerText = "Reload";
  button.onclick = () => {
    clearMessages();
    location.reload();
    return false;
  };
  appendMessageElement(button);
}

/**
 * Append a message HTML element to the message list
 * @param {HTMLElement} element
 */
function appendMessageElement(element) {
  messageContainer.appendChild(element);
  messageContainer.scrollTop = messageContainer.scrollHeight;

  // Remove old messages
  while (messageContainer.childElementCount > MESSAGE_HISTORY_SIZE) {
    messageContainer.firstChild?.remove();
  }
}

/**
 * Clear all message history
 */
function clearMessages() {
  messageContainer.innerHTML = "";
}

/**
 * Disable chat input
 */
function disableChat() {
  messageInput.disabled = true;
  sendButton.disabled = true;
  chatForm.disabled = true;
}

/**
 * Get an element by id and throw if not found
 * @param {string} id
 * @returns {HTMLElement}
 */
function getById(id) {
  const element = document.getElementById(id);

  if (!element) {
    throw new Error(`No element with id '${id}'.`);
  }

  return element;
}
