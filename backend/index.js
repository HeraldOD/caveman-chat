import { WebSocket, WebSocketServer } from "ws";
import {
  findUser,
  getUserListMessage,
  heartbeat,
  log,
  validateUsername,
} from "./utils.js";

const HOST = process.env.HOST ?? "0.0.0.0";
const PORT = process.env.PORT ?? 9600;
const CONNECTION_PRUNE_INTERVAL = 10000;

log(`Starting on ${HOST}:${PORT}`);
const wss = new WebSocketServer({ host: HOST, port: PORT });
let connections = [];

wss.on("connection", (ws, req) => {
  log(`Incoming connection : ${req.socket.remoteAddress}`);
  connections.push(ws);
  log(`Connected: ${connections.length}`);

  ws.on("message", (data) => {
    handleMessage(data.toString("utf8"), ws);
  });

  ws.on("error", console.error);

  ws.on("close", () => {
    removeConnection(ws);
  });

  ws.on("pong", heartbeat);
});

/**
 * Handle an incoming message
 * @param {string} raw Raw message contents
 * @param {WebSocket} ws
 */
function handleMessage(raw, ws) {
  log(`MSG: ${raw}`);

  try {
    const data = JSON.parse(raw);

    switch (data.type) {
      case "message":
        // Enforce server side username
        data.username = ws.username;
        broadcastString(JSON.stringify(data));
        break;
      case "join":
        handleJoin(ws, data.username);
        break;
      case "command":
        handleCommand(ws, data.content);
        break;
    }
  } catch (e) {
    console.error(e);
  }
}

/**
 * Handle a new user joining
 * @param {WebSocket} ws User's socket
 * @param {string} username Chosen username
 * @returns
 */
function handleJoin(ws, username) {
  if (ws.username === username) {
    sendSystemMessage("You have already joined.", ws);
    return;
  }

  if (connections.filter((c) => c.username === username).length > 0) {
    sendSystemMessage("That username is already in use.", ws);
    ws.close();
    return;
  }

  if (!validateUsername(username)) {
    sendSystemMessage("Invalid username.", ws);
    ws.close();
    return;
  }

  sendSystemMessage(getUserListMessage(connections), ws);

  ws.username = username;
  sendSystemMessage(`+ User ${username} has joined.`);
}

/**
 * Handle a command
 * @param {WebSocket} ws
 * @param {string} rawCommand
 */
function handleCommand(ws, rawCommand) {
  const args = rawCommand.split(" ");

  if (args.length === 0) {
    sendSystemMessage("Empty command.", ws);
    return;
  }

  const command = args[0];
  switch (command) {
    case "help":
      sendSystemMessage("No.", ws);
      break;
    case "ping":
      sendSystemMessage("Pong!", ws);
      break;
    case "list":
      sendSystemMessage(getUserListMessage(connections), ws);
      break;
    case "time":
      const time = new Date().toLocaleTimeString();
      sendSystemMessage(`Server time: ${time}`, ws);
      break;
    case "exit":
    case "kill":
      ws.close();
      break;
    case "whisper":
    case "msg":
      whisper(args[1], args.slice(2).join(" "), ws);
      break;
    case "kick":
      kick(args[1], ws);
      break;
    default:
      sendSystemMessage(`Unknown command '/${command}'.`, ws);
      break;
  }
}

/**
 * Send a private message to a user
 * @param {string} username
 * @param {string} message
 * @param {WebSocket} ws Current user socket
 */
function whisper(username, message, ws) {
  if (!username || !message) {
    sendSystemMessage(`Usage: /whisper <user> <message>`, ws);
    return;
  }

  const targetWs = findUser(username, connections);
  if (!targetWs) {
    sendSystemMessage(`Unknown user.`, ws);
    return;
  }

  sendSystemMessage(`You whisper to ${username}: ${message}`, ws);
  sendSystemMessage(`${ws.username} whispers: ${message}`, targetWs);
}

/**
 * Kick a user
 * @param {string} username
 * @param {WebSocket} ws Current user socket
 */
function kick(username, ws) {
  if (!username) {
    sendSystemMessage(`Usage: /kick <user>`, ws);
    return;
  }

  const targetWs = findUser(username, connections);
  if (!targetWs) {
    sendSystemMessage(`Unknown user.`, ws);
    return;
  }

  if (targetWs === ws) {
    sendSystemMessage(`You kicked yourself. Nice job.`, ws);
  } else {
    sendSystemMessage(`You were kicked by ${ws.username}.`, targetWs);
  }
  targetWs.close();
}

/**
 * Close a connection and remove it from list of connections
 * @param {WebSocket} ws
 */
function removeConnection(ws) {
  if (ws.readyState === ws.OPEN) {
    ws.close();
  }

  if (ws.username) {
    sendSystemMessage(`- User ${ws.username} has left.`);
  }

  connections = connections.filter((x) => x !== ws);
  log(`Connection closed: '${ws.username}'`);
  log(`Connected: ${connections.length}`);
}

/**
 * Send a system message in the chat
 * @param {string} messageContent Text content of the message
 * @param {WebSocket | undefined} ws Socket to send the message to (optional) - if left
 * undefined, message is broadcasted to all sockets
 */
function sendSystemMessage(messageContent, ws = undefined) {
  const data = {
    type: "system",
    content: messageContent,
  };

  const str = JSON.stringify(data);
  if (ws) {
    ws.send(str);
  } else {
    broadcastString(str);
  }
}

/**
 * Broadcast a string to all connections
 * @param {string} str
 */
function broadcastString(str) {
  connections
    // avoid leaking messages to non-authenticated sockets
    .filter((ws) => ws.username)
    .forEach((ws) => {
      ws.send(str);
    });
}

// Prune dead connections every once in a while
const pruneInterval = setInterval(() => {
  connections.forEach((ws) => {
    if (ws.isAlive === false) {
      return ws.terminate();
    }

    ws.isAlive = false;
    ws.ping();
  });
}, CONNECTION_PRUNE_INTERVAL);

function exit() {
  console.info("Stopping.");
  clearInterval(pruneInterval);
  wss.close();
  process.exit();
}

process.on("SIGINT", exit);
process.on("SIGTERM", exit);
