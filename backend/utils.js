const usernameRegex = /[a-zA-Z][a-zA-Z0-9_]+/;

/**
 * Log a message to the console
 * @param {string} message
 */
export function log(message) {
  const date = new Date().toISOString();
  console.log(`[${date}] ${message}`);
}

/**
 * Check if a username is valid
 * @param {string} username
 */
export function validateUsername(username) {
  const matches = username.match(usernameRegex);
  return matches && matches[0] === username;
}

/**
 * Get the list of connected usernames
 * @param {any[]} connections
 * @returns {string[]}
 */
export function getUserList(connections) {
  return connections.filter((c) => c.username).map((c) => c.username);
}

/**
 * Get a message listing connected usernames
 * @param {any[]} connections
 * @returns {string}
 */
export function getUserListMessage(connections) {
  const usernames = getUserList(connections);

  if (usernames.length > 0) {
    return `${usernames.length} user(s) online: ${usernames.join(", ")}`;
  } else {
    return "No one else is online.";
  }
}

/**
 * Get the web socket connection associated to a username
 * @param {string} username
 * @param {any[]} connections
 */
export function findUser(username, connections) {
  const ws = connections.find((x) => x.username === username);
  return ws;
}

export function heartbeat() {
  this.isAlive = true;
}
