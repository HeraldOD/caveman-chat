# Caveman Chat

Really simple websocket chat server + web client

## Start Using Containers

```sh
docker-compose up
```

## Bare Metal

### Server

```sh
cd backend
npm install
node .
```

### Frontend

Host the files in `frontend/public/` with any web server
